package com.bahaida.economasnim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EconomaSnimApplication {

	public static void main(String[] args) {
		SpringApplication.run(EconomaSnimApplication.class, args);
	}
}
